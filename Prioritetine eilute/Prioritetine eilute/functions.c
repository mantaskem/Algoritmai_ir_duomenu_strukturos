#include <stdio.h>
#include <stdlib.h>
#include "functions.h"

Queue *InitQueue()//sukuria tuscia prioritetine eilute
{
    Queue *q=(Queue*)malloc(sizeof(Queue));
    q->rear = NULL;
    q->front=NULL;
    
    return q;
}
void Insert(Queue *q,int d,int p)
{
    Node *newNode = malloc(sizeof(Node));
    newNode->data = d;
    newNode->p = p;
    newNode->next = NULL;
    
    if(!IsEmpty(q))
    {
        if(newNode->p > q->front->p)//jei nauja reiksme yra didesne uz pirma prioriteta kuris jau yra irasytas
        {
            newNode->next = q->front;
            q->front = newNode;
            q->rear = newNode;
        }
        //neveikia
        else
        {
            Node *prevTemp = q->front;
            Node *temp = q->front->next;
            while(temp != NULL )
            {
                if(temp->p < newNode->p) break;
                prevTemp = temp;
                temp = temp->next;
            }
            if(temp == NULL)
            {
                prevTemp->next = newNode;
                q->rear = newNode;
            }
            else
            {
                newNode->next = temp;
                prevTemp->next = newNode;
            }
        }
    }
    else
    {
        q->front = newNode;
        q->rear = newNode;
    }
    
}
int IsEmpty(Queue*q)
{
    
    if(q->rear==NULL )
        return 1; // jei tuscias
    
    return 0; // jei netuscias
    
}
int Delete(Queue *q)
{
    int kiek = 0;
    Node*tempr=q->front;
    while(tempr)
    {
        kiek++;
        tempr=tempr->next;
    }

    if(IsEmpty(q))
    {
        return 0;
    }
    
    if(kiek > 1)
    {
        Node *temp = q->front;
        q->front = q->front->next;
        int topNum = q->front->p;
        free(temp);
        return topNum;
    }
    else
    {
        Destroy(q);
        return 0;
    }
    
}
void Print(Queue*q)
{
    printf("Priorities: ");
    
    Node*temp=q->front;
    while(temp)
    {
        printf("%d ",temp->p);
        temp=temp->next;
    }
    printf("\n");
    
    printf("Elements: ");
    
    Node*tempr=q->front;
    while(tempr)
    {
        printf("%d ",tempr->data);
        tempr=tempr->next;
    }
    printf("\n");
}
void Destroy(Queue *q)
{
    q->rear = NULL;
    q->front=NULL;
    free(q);
}
int Search(Queue *q, int ser)
{
    int sk = 0;
    Node*temp=q->front;
    
    while(temp)
    {
        if(temp->data == ser)
        {
            sk++;
        }
        temp=temp->next;
    }
    return sk;
}

