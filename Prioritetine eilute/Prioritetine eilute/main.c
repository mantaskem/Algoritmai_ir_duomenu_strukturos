#include<stdio.h>
#include<stdlib.h>
#include "functions.h"

int main()
{
    int data, priority, koks, yra = 1, or;
    Queue *q=InitQueue();
    
    while(yra)
    {
        printf("-----================ MENU ================-----\n");
        printf("[1] Ivesti naujus prioritetines eiles elmentus.\n");
        printf("[2] Istrinti elementa su didziausiu prioritetu.\n");
        printf("[3] Atspausdinti prioritetine eile.\n");
        printf("[4] Patikrinti ar prioritetine eile tuscia.\n");
        printf("[5] Surasti kiek yra elementu eileje.\n");
        printf("[6] Sunaikinti prioritetine eile.\n");
        printf("[7] Baigti operacijas su prioritetine eile.\n\n");
        
        printf("Iveskite savo pasirinkima: ");
        scanf("%d", &koks);
        printf("\n\n");
    
        switch (koks) {
            case 1:
                printf("Ivesti reiksme: ");
                scanf("%d", &data);
                printf("Ivesti jo prioriteta: ");
                scanf("%d", &priority);
                Insert(q,data,priority);
                printf("*** Elementas buvo iterptas ***\n\n");
                break;
            case 2:
                Delete(q);
                printf("*** Elementas buvo istrintas ***\n\n");
                break;
            case 3:
                Print(q);
                printf("\n\n");
                break;
            case 4:
                or = IsEmpty(q);
                if (or == 1) printf("*** Eile yra tuscia ***\n\n");
                else printf("*** Eile yra netuscia ***\n\n");
                break;
            case 5:
                printf("Ivesti ieskomo elemento: ");
                scanf("%d", &data);
                printf("\n*** Elementas %d buvo parasytas %d kartu ***\n\n",data, Search(q, data));
                break;
            case 6:
                Destroy(q);
                printf("*** Eile buvo sunaikinta ***\n\n");
                break;
            default:
                yra = 0;
                break;
        }
    }
    
    return 0;
    
}
