#ifndef functions_h
#define functions_h

typedef struct node
{
    int data;
    struct node *next;
    int p;
}Node;

typedef struct queue
{
    struct node *rear, *front;
}Queue;


//-------====== FUNCTIONS ==============-----------

Queue *InitQueue(); //inicializacija
void Insert(Queue *q,int d,int p); //idejimas i sarasa elemento ir prioriteto
int IsEmpty(Queue *q); //Ar tuscias: Jei tuscias grazina: 1, jei ne tuscias 0.
int Delete(Queue *q);  //Istrinimas pirmo elemento su didziausiu prioritetu, jei prioritetai lygus seniausiai ikelto. Grazina reiksme rodancia i sekanti pagal diduma prioriteta.
void Print(Queue *q);  //Spausdinimas
void Destroy(Queue *q);//Sekos naikinimas.
int Search(Queue *q, int ser);//paieska
//-------===============================-----------

#endif
