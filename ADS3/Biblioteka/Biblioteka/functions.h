//
//  functions.h
//  Biblioteka
//
//  Created by Mantas Kemėšius on 13/04/17.
//  Copyright © 2017 Mantas Kemėšius. All rights reserved.
//

#ifndef functions_h
#define functions_h

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//Medzio
struct node
{
    int value;
    void* data;
    int height;
    struct node * left;
    struct node * right;
};

typedef struct node * avl;
typedef struct node Node;

//Vektoriaus
typedef int Object;

typedef struct Vektoris{
    Object duomenys;
    struct Vektoris *kitas;
}Struktura;

typedef struct Elementas{
    Struktura *pradzia;
    Struktura *pabaiga;
    int sukurtas;
}Vektorius;
void quicksort(int x[300],int first,int last);
//Reikalingos man funkcijos:
void Skaitymas (Node *n, int *sk_atvykimas, int *sk_knyga);
int SearchV(Vektorius *n, int tarp, int s);
int Random(int max);

// MEDZIO
Node* insertNode(Node*, int, void*, int*); // err == 1, if already exists
Node* deleteNode(Node*, int, int*); //err == 1, Node with key n, not found
Node* findNode(Node*, int, int *kiek); // returns NULL if not found
int avlSize(Node*); // counts all nodes
void preOrder(int[], Node*);
void inOrder(int[], Node*);
void postOrder(int[], Node*);



//Vektorio
int Vector(Vektorius *pirmas);
int isEmpty(Vektorius pirmas);
int removeAllElements(Vektorius *pirmas);
int removeElementAt(Vektorius *pirmas, int index);
int insertElementAt(Vektorius *pirmas, Object x, int index);
int addElement(Vektorius *pirmas, Object x);
int setElementAt(Vektorius *pirmas, Object x, int index);
int elementAt(Vektorius pirmas, int index, Object *x);
int size(Vektorius pirmas, int *kiekis);

#endif
