#include "functions.h"

int main() {
    
    int *sk_atvykimas_t, *sk_knyga_t, err = 1;
    int tmp, size = 0;
    
    avl root = NULL;
    Vektorius pirmas, antras;
    Vector(&pirmas);
    Vector(&antras);
    
    FILE *file;
    file = fopen("duom.txt", "r");

    fscanf(file, "%d %d", &sk_atvykimas_t, &sk_knyga_t);
    while (!feof (file))
    {
        fscanf(file, "%d", &tmp);
        root = insertNode(root, size, &tmp, &err);
        insertElementAt(&pirmas, tmp, size);
        size++;
    }
    
    fclose(file);
     
    int arr[size], arr2[size]; size--;
    Struktura *el1 = pirmas.pradzia;
    
    for ( int i = 0 ; i < size; i++ )
    {
        arr2[i] = el1->duomenys;
        arr[i] = el1->duomenys;
        el1 = el1->kitas;
    }
    
    //Rikiavimas
    quicksort(arr,0,size-1);
    
   // for ( int i = 0 ; i < size; i++ )
    //{
     //   printf("%d ", arr[i]);
    //}
    
    for ( int i = 1 ; i <= size; i++ )
    {
        insertElementAt(&antras, arr[i-1], i);
    }
    
    int kiekis1[size], kiekis2[size], kiekis3, kiekis4[size], APTAR[3], tarp = 0, bet1, bet2;
    int skaic = 342, darb = 0, skaityt, VISO = 0, patiks = 0;
    APTAR[0] = 0; APTAR[1] = 0; APTAR[2] = 0;
    
    //KIEK ZMONIU BUS PER VIENA VALANDA
    int skai = sk_atvykimas_t;
    int skai2 = sk_knyga_t;
    
    int k = 0;
    
    
    srand (time(NULL));
    for( int z = 0 ; z < 100; z++ )//tikslesniam rezultatui
    {
    darb = 0;
    tarp = 0; k = 0;
    bet1 = skai * size;
    bet1 = bet1 / 100;
    //************************************ NERIKIUOTAS ************************************
    while (tarp != 420)//420 min
    {
        bet1 = Random(bet1);// Kiek atvyks per valanda zmoniu
        skaityt = bet1/6;// kas 10 min kiek ateina zmoniu ir kiek reikia darbuotoju
        
        if (skaityt == 0)
        {
            darb = 0;
            skaityt = 1;
        }
        
        if ( skaityt >= darb)
        {
            APTAR[0] += skaityt - darb;
            darb = 0;
        }
        else
        {
            darb -= skaityt;
        }
        
        int min = 0;
        
        while(min != 60)
        {
            if( min == 0 || min == 10 || min == 20 || min == 30 || min == 40 || min == 50 )
            {
                for ( int i = 0 ; i < skaityt; i++ )//per visus skaitytojus kas 10 min
                {
                    //TIKIMYBE DEL KNYGOS AR TURES
                    if ( skai2 < 100 )// jei tikimybe mazesne uz 100 tik tada ziurim, nes kitaip uztikrintai gaus.
                    {
                        bet2 = 100 - skai2;
                        bet2 = (bet2 * size)/ 100;
                        bet2 = Random(bet2+size);//knygos numeris
                    }
                    else
                    {
                        bet2 = Random(size);//knygos numeris
                    }
                
                    if (bet2 <= size)
                        bet2 = arr2[bet2];
                    else
                        bet2 = -1;//neegzistuojanti knyga
                    
                    for ( int i = 0 ; i < k; i++ )
                    {
                        if (kiekis1[i] == -1 )
                        {
                            patiks++;
                        }
                    }
                
                    if (patiks > 0)
                    {
                        for ( int i = 0 ; i < k; i++ )
                        {
                            if (kiekis1[i] == -1 )
                            {
                                patiks = i;
                                break;
                            }
                        }
                        kiekis1[patiks] = SearchV(&pirmas, bet2, size);// visu laikai grazina per kiek minuciu aptarnaus
                    }
                    else
                    {
                        kiekis1[k] = SearchV(&pirmas, bet2, size);
                        k++;
                    }
                }
            }
            
            for ( int i = 0 ; i < k; i++ )
            {
                //printf("Laikas: %d ir %d\n", tarp, kiekis1[i]);
                if (kiekis1[i] != 0 && kiekis1[i] != -1)//darbuotojo atsilaisvinimas.
                {
                    kiekis1[i] -= 1;
                }
                else
                {
                    if ( kiekis1[i] == 0)
                    {
                        kiekis1[i] = -1;
                        darb++;
                    }
                }
            }
            min++;
            tarp++;
        }
    }
    
    darb = 0;
    tarp = 0; k = 0;
    bet1 = skai * size;
    bet1 = bet1 / 100;
    
    //************************************ RIKIUOTAS ************************************
    while (tarp != 420)//420 min
    {
        bet1 = Random(bet1);// Kiek atvyks per valanda zmoniu
        skaityt = bet1/6;// kas 10 min kiek ateina zmoniu ir kiek reikia darbuotoju
        
        if (skaityt == 0)
        {
            darb = 0;
            skaityt = 1;
        }
        
        if ( skaityt >= darb)
        {
            APTAR[1] += skaityt - darb;
            darb = 0;
        }
        else
        {
            darb -= skaityt;
        }
        
        int min = 0;
        
        while(min != 60)
        {
            if( min == 0 || min == 10 || min == 20 || min == 30 || min == 40 || min == 50 )
            {
                for ( int i = 0 ; i < skaityt; i++ )//per visus skaitytojus kas 10 min
                {
                    //TIKIMYBE DEL KNYGOS AR TURES
                    if ( skai2 < 100 )// jei tikimybe mazesne uz 100 tik tada ziurim, nes kitaip uztikrintai gaus.
                    {
                        bet2 = 100 - skai2;
                        bet2 = (bet2 * size)/ 100;
                        bet2 = Random(bet2+size);//knygos numeris
                    }
                    else
                    {
                        bet2 = Random(size);//knygos numeris
                    }
                    
                    if (bet2 <= size)
                        bet2 = arr2[bet2];
                    else
                        bet2 = -1;//neegzistuojanti knyga
                    
                    for ( int i = 0 ; i < k; i++ )
                    {
                        if (kiekis1[i] == -1 )
                        {
                            patiks++;
                        }
                    }
                    
                    if (patiks > 0)
                    {
                        for ( int i = 0 ; i < k; i++ )
                        {
                            if (kiekis1[i] == -1 )
                            {
                                patiks = i;
                                break;
                            }
                        }
                        kiekis2[patiks] = SearchV(&antras, bet2, size);// visu laikai grazina per kiek minuciu aptarnaus
                    }
                    else
                    {
                        kiekis2[k] = SearchV(&antras, bet2, size);
                        k++;
                    }
                }
            }
            
            for ( int i = 0 ; i < k; i++ )
            {
                //printf("Laikas: %d ir %d\n", tarp, kiekis1[i]);
                if (kiekis2[i] != 0 && kiekis2[i] != -1)//darbuotojo atsilaisvinimas.
                {
                    kiekis2[i] -= 1;
                }
                else
                {
                    if ( kiekis2[i] == 0)
                    {
                        kiekis2[i] = -1;
                        darb++;
                    }
                }
            }
            min++;
            tarp++;
        }
    }
    
    darb = 0;
    tarp = 0; k = 0;
    bet1 = skai * size;
    bet1 = bet1 / 100;
    
        int patiks = 0;
    //************************************ MEDIS ************************************
    while (tarp != 420)//420 min
    {
        bet1 = Random(bet1);// Kiek atvyks per valanda zmoniu
        skaityt = bet1/6;// kas 10 min kiek ateina zmoniu ir kiek reikia darbuotoju
        
        if (skaityt == 0)
        {
            darb = 0;
            skaityt = 1;
        }
        
        if ( skaityt >= darb)
        {
            APTAR[2] += skaityt - darb;
            darb = 0;
        }
        else
        {
            darb -= skaityt;
        }
        
        int min = 0;
        
        while(min != 60)
        {
            if( min == 0 || min == 10 || min == 20 || min == 30 || min == 40 || min == 50 )
            {
                for ( int i = 0 ; i < skaityt; i++ )//per visus skaitytojus kas 10 min
                {
                    //TIKIMYBE DEL KNYGOS AR TURES
                    if ( skai2 < 100 )// jei tikimybe mazesne uz 100 tik tada ziurim, nes kitaip uztikrintai gaus.
                    {
                        bet2 = 100 - skai2;
                        bet2 = (skai2 * size)/100;
                        bet2 = Random(skai2+size);
                    }
                    else
                    {
                        bet2 = Random(size);
                    }
                    
                    if ( bet2 > size)
                    {
                        bet2 = -1;
                    }
                    else
                    {
                        bet2 = arr[bet2];
                    }
                    
                    for ( int i = 0 ; i < size; i++ )
                    {
                        if (kiekis4[i] == -1)
                            patiks++;
                    }
                    
                    if (patiks > 0)
                    {
                        for ( int i = 0 ; i < k; i++ )
                        {
                            if (kiekis4[i] == -1 )
                            {
                                patiks = i;
                                break;
                            }
                        }
                        findNode(root, bet2, &kiekis3);
                        kiekis4[patiks] = kiekis3;// visu laikai grazina per kiek minuciu aptarnaus
                    }
                    else
                    {
                        findNode(root, bet2, &kiekis3);
                        kiekis4[k] = kiekis3;
                        k++;
                    }
                }
            }
            
            for ( int i = 0 ; i < k; i++ )
            {
                //printf("Laikas: %d ir %d\n", tarp, kiekis1[i]);
                if (kiekis4[i] != 0 && kiekis4[i] != -1)//darbuotojo atsilaisvinimas.
                {
                    kiekis4[i] -= 1;
                }
                else
                {
                    if ( kiekis4[i] == 0)
                    {
                        kiekis4[i] = -1;
                        darb++;
                    }
                }
            }
            min++;
            tarp++;
        }
    }
    darb = 0;
    tarp = 0; k = 0;
    bet1 = skai * size;
    bet1 = bet1 / 100;
    
}
    
    printf("Kiek papildomai darbuotoju reikejo samdyti:\n");
    printf("%d -- Nerikiuotas\n", APTAR[0]);
    printf("%d -- Rikiuotas\n", APTAR[1]);
    printf("%d -- Medis\n", APTAR[2]);
    
    
    return 0;
}
