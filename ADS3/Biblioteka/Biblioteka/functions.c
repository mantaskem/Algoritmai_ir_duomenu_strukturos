#include "functions.h"


void quicksort(int x[300],int first,int last){
    int pivot,j,temp,i;
    
    if(first<last){
        pivot=first;
        i=first;
        j=last;
        
        while(i<j){
            while(x[i]<=x[pivot]&&i<last)
            i++;
            while(x[j]>x[pivot])
            j--;
            if(i<j){
                temp=x[i];
                x[i]=x[j];
                x[j]=temp;
            }
        }
        
        temp=x[pivot];
        x[pivot]=x[j];
        x[j]=temp;
        quicksort(x,first,j-1);
        quicksort(x,j+1,last);
        
    }
}



int Random(int max){
    
    return rand() % max +1;
}

int SearchV(Vektorius *n, int tarp, int s)
{
    Struktura *elementas = n->pradzia;
    int i = 0;
    
    while(elementas != NULL)
    {
        if ( elementas->duomenys == tarp)
        {
            return i;
        }
        elementas = elementas->kitas;
        i++;
    }
    
    return i;
}


/*--------------------------------------------------------------------
 *********************** MEDIS **************************
 *----------------------------------------------------------------------*/


int height(Node* n)
{
    if (n == NULL)
    {
        return 0;
    }
    return n->height;
}

Node* leftRotate(Node* n)
{
    Node* x = n->right;
    Node* T = x->left;
    
    x->left = n;
    n->right = T;
    
    n->height = (height(n->left) > height(n->right) ? height(n->left) : height(n->right)) + 1;
    x->height = (height(x->left) > height(x->right) ? height(x->left) : height(x->right)) + 1;
    
    return x;
}

Node* rightRotate(Node* n)
{
    Node* x = n->left;
    Node* T = x->right;
    
    x->right = n;
    n->left = T;
    
    n->height = (height(n->left) > height(n->right) ? height(n->left) : height(n->right)) + 1;
    x->height = (height(x->left) > height(x->right) ? height(x->left) : height(x->right)) + 1;
    
    return x;
}

int countBalance(Node* n)
{
    return (n != NULL ? (height(n->left) - height(n->right)) : 0);
}

Node* newNode(Node* n, int val, void* data)
{
    n = (Node*)malloc(sizeof(Node));
    
    n->value = val;
    n->data = data;
    n->height = 1;
    n->left = NULL;
    n->right = NULL;
    
    return n;
}

Node* insertNode(Node* n, int val, void* data, int* err)
{
    if (n == NULL)
    {
        *err = 0;
        return newNode(n, val, data);
    }
    
    if (val < n->value)
    {
        n->left = insertNode(n->left, val, data, err);
    }
    else if (val > n->value)
    {
        n->right = insertNode(n->right, val, data, err);
    }
    else
    {
        *err = 1;
        return n;
    }
    
    n->height = (height(n->left) > height(n->right) ? height(n->left) : height(n->right)) + 1;
    int balanceFactor = countBalance(n);
    
    //left left
    if (balanceFactor > 1 && val < n->left->value)
    {
        return rightRotate(n);
    }
    //right right
    if (balanceFactor < -1 && val > n->right->value)
    {
        return leftRotate(n);
    }
    //left right
    if (balanceFactor > 1 && val > n->left->value)
    {
        n->left = leftRotate(n->left);
        return rightRotate(n);
    }
    //right left
    if (balanceFactor < -1 && val < n->right->value)
    {
        n->right = rightRotate(n->right);
        return leftRotate(n);
    }
    
    return n;
}

Node* deleteNode(Node* n, int val, int* err)
{
    *err = 1;
   	if (n == NULL)
    {
        return n;
    }
    
    if (val < n->value)
    {
        n->left = deleteNode(n->left, val, err);
    }
    else if (val > n->value)
    {
        n->right = deleteNode(n->right, val, err);
    }
    else
    {
        *err = 0;
        if (n->left == NULL && n->right == NULL)
        {
            Node* temp = NULL;
            temp = n;
            n = NULL;
            free(temp);
        }
        else if(n->left == NULL || n->right == NULL)
        {
            Node* temp = (n->left != NULL ? n->left : n->right);
            *n = *temp;
            free(temp);
        }
        else
        {
            Node* temp = n->right;
            
            while (temp->left != NULL)
            {
                temp = temp->left;
            }
            
            n->value = temp->value;
            
            n->right = deleteNode(n->right, temp->value, err);
        }
    }
    
    if (n == NULL)
    {
        return n;
    }
    
    n->height = (height(n->left) > height(n->right) ? height(n->left) : height(n->right)) + 1;
    int balanceFactor = countBalance(n);
    
    // Left Left Case
    if (balanceFactor > 1 && countBalance(n->left) >= 0)
    {
        return rightRotate(n);
    }
    // Right Right Case
    if (balanceFactor < -1 && countBalance(n->right) <= 0)
    {
        return leftRotate(n);
    }
    // Left Right Case
    if (balanceFactor > 1 && countBalance(n->left) < 0)
    {
        n->left = leftRotate(n->left);
        return rightRotate(n);
    }
    // Right Left Case
    if (balanceFactor < -1 && countBalance(n->right) > 0)
    {
        n->right = rightRotate(n->right);
        return leftRotate(n);
    }
    
    return n;
}

void preTraversal(int values[], Node* n, int* index)
{
    if (n != NULL)
    {
        values[*index] = n->value;
        (*index)++;
        
        preTraversal(values, n->left, index);
        preTraversal(values, n->right, index);
    }
}

void preOrder(int values[], Node* n)
{
    int i = 0;
    preTraversal(values, n, &i);
}

void inTraversal(int values[], Node* n, int* index)
{
    if (n != NULL)
    {
        inTraversal(values, n->left, index);
        
        values[*index] = n->value;
        (*index)++;
        
        inTraversal(values, n->right, index);
    }
}

void inOrder(int values[], Node* n)
{
    int i = 0;
    inTraversal(values, n, &i);
}

void postTraversal(int values[], Node* n, int* index)
{
    if (n != NULL)
    {		
        postTraversal(values, n->left, index);		
        postTraversal(values, n->right, index);
        
        values[*index] = n->value;
        (*index)++;
    }
}

void postOrder(int values[], Node* n)
{
    int i = 0;
    postTraversal(values, n, &i);
}

Node* findNode(Node* n, int val, int *kiek)
{
    //printf("\nKIEK:%d ------", *kiek);
    Node* current = n;
    if (current == NULL)
    {
        *kiek+=1;
       // printf("1kiek: %d\n", *kiek);
        return NULL;
    }
    
    if (val < current->value)
    {
        *kiek+=1;
        //printf("2kiek: %d\n", *kiek);
        current = findNode(n->left, val, kiek);
    }
    else if (val > current->value)
    {
        *kiek+=1;
        //printf("3kiek: %d\n", *kiek);
        current = findNode(n->right, val, kiek);
    }
    else
    {
        *kiek+=1;
        //printf("4kiek: %d\n", *kiek);
        return current;
    }

    return current;
}

int avlSize(Node* n)
{
    if (n != NULL)
    {				
        return (avlSize(n->left) + avlSize(n->right) + 1);
    }
    
    return 0;
}


/*--------------------------------------------------------------------
*********************** VEKTORIUS **************************
*----------------------------------------------------------------------*/


int Vector(Vektorius *pirmas)
{
    
    if((*pirmas).sukurtas == 0)
    {
        (*pirmas).sukurtas = 1;
        (*pirmas).pradzia = NULL;
        (*pirmas).pabaiga = NULL;
        return(0);
    }
    else
    {
        return(1);
    }
}

int isEmpty(Vektorius pirmas)
{
    int kiekis;
    size(pirmas, &kiekis);
    if(pirmas.sukurtas == 0)return 2;
    else if(kiekis>0)return 0;
    else return 1;
}

int removeAllElements(Vektorius *pirmas)
{
    if((*pirmas).sukurtas == 0)
    {
        return 2;
    }
    Struktura *elementas1,*elementas2;
    elementas1 = (*pirmas).pradzia;
    while(elementas1 != NULL)
    {
        elementas2 = elementas1->kitas;
        free(elementas1);
        elementas1 = elementas2;
    }
    (*pirmas).pabaiga = NULL;
    (*pirmas).pradzia = NULL;
    (*pirmas).sukurtas = 0;
    if((*pirmas).sukurtas == 0) return 0;
    else return 1;
}

int removeElementAt(Vektorius *pirmas, int index)
{
    Struktura *elem,*elem2;
    if((*pirmas).sukurtas == 0)
    {
        return 2;
    }
    if((*pirmas).pradzia == NULL)
    {
        return 1;
    }
    else
    {
        elem = (*pirmas).pradzia;
        if(index == 0)
        {
            elem = (*pirmas).pradzia -> kitas;
            (*pirmas).pradzia = elem;
            (*pirmas).pabaiga = elem;
            return 0;
        }
        while(index-1 > 0 && elem -> kitas != NULL)
        {
            index--;
            elem = elem->kitas;
        }
        if(elem -> kitas == NULL)
        {
            return 1;
        }
        elem2 = elem;
        elem = elem -> kitas;
        elem2 -> kitas = elem ->kitas;
        if(elem ->kitas == NULL)(*pirmas).pabaiga = elem2;
        free(elem);
        return 0;
    }
    
}

int insertElementAt(Vektorius *pirmas, Object x, int index)
{
    if((*pirmas).sukurtas == 0)
    {
        return 2;
    }
    Struktura *elem, *elem2;
    int i;
    elem2 = (*pirmas).pradzia;
    elem = (Struktura*)malloc(sizeof(Struktura));
    if(elem == NULL)return 1;
    if((*pirmas).pradzia == NULL)
    {
        if(index != 0)
        {
            return 1;
        }
        elem->duomenys = x;
        elem->kitas = (*pirmas).pradzia;
        (*pirmas).pradzia = elem;
        (*pirmas).pabaiga = elem;
        return 0;
    }
    else
    {
        for(i = 0; i < index-1; i++)
        {
            elem2 = elem2 -> kitas;
        }
        elem->duomenys = x;
        elem->kitas = elem2 ->kitas;
        elem2 ->kitas = elem;
        if(elem->kitas == NULL)(*pirmas).pabaiga = elem;
        return 0;
    }
}

int addElement(Vektorius *pirmas, Object x)
{
    if((*pirmas).sukurtas == 0)
    {
        return 2;
    }
    Struktura *elem1, *elem2;
    elem2 = (*pirmas).pabaiga;
    elem1 = (Struktura*)malloc(sizeof(Struktura));
    if(elem1 == NULL)
    {
        return 1;
    }
    elem1 -> duomenys = x;
    elem1 -> kitas = NULL;
    if((*pirmas).pradzia == NULL)
    {
        (*pirmas).pradzia = elem1;
        (*pirmas).pabaiga = elem1;
        return 0;
    }
    (*pirmas).pabaiga -> kitas = elem1;
    (*pirmas).pabaiga = elem1;
    return 0;
}

int setElementAt(Vektorius *pirmas, Object x, int index)
{
    if((*pirmas).sukurtas == 0)
    {
        return 2;
    }
    Struktura *elem;
    elem = (*pirmas).pradzia;
    while(index > 0 && elem != NULL)
    {
        elem = elem -> kitas;
        index--;
    }
    if(elem == NULL)
    {
        return 1;
    }
    else
    {
        elem->duomenys = x;
        return 0;
        
    }
    
}

int elementAt(Vektorius pirmas, int index, Object *x)
{
    if(pirmas.sukurtas == 0)
    {
        return 2;
    }
    Struktura *elem;
    elem = pirmas.pradzia;
    while(index > 0)
    {
        elem = elem -> kitas;
        index--;
    }
    if(elem == NULL)
    {
        printf("Klaida, nera jokio elemento nurodytoje vietoje\n");
        return 1;
    }
    *x = elem ->duomenys;
    return 0;
}

int size(Vektorius pirmas, int *kiekis)
{
    if(pirmas.sukurtas == 0)return 1;
    int i = 1;
    Struktura *elem;
    if(pirmas.pradzia == NULL)
    {
        *kiekis = 0;
        return 0;
    }
    else
    {
        elem = pirmas.pradzia;
        while(elem->kitas != NULL)
        {
            elem = elem->kitas;
            i++;
        }
        *kiekis = i;
    }
    return 0;
}
